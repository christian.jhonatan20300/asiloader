### Generate bass_sym

1. Copy file `libbass.so` to project directory
2. Execute command below:

```bash
objdump -T libbass.so | grep BASS_ | grep -v Java | cut -c 33- | awk '{print "extern \"C\" __attribute__((visibility(\"default\"))) void " $1 "();"}' > sources/bass_sym.h && cat sources/bass_sym.h | wc -l | awk '{print "constexpr int g_bassCount = " $1 ";"}' >> sources/bass_sym.h
echo "#include \"bass_sym.h\"\n#include \"log.h\"\n" > sources/bass_sym.cpp && objdump -T libbass.so | grep BASS_ | grep -v Java | cut -c 33- | awk '{print "void " $1 "(){FATAL(\"Direct calling dummy of " $1 "\");}"}' >> sources/bass_sym.cpp
```

 ### Generate bass_bin

1. Copy file `libbass.so` to project directory
2. Execute command below:

```bash
xxd -i libbass.so > sources/base_bin.h
```



